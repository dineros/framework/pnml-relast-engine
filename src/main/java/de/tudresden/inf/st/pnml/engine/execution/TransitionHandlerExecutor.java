package de.tudresden.inf.st.pnml.engine.execution;

import de.tudresden.inf.st.pnml.jastadd.model.BalloonToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Automatically sort and call the callbacks.
 */
public class TransitionHandlerExecutor {

    protected static final Logger logger = LoggerFactory.getLogger(TransitionHandlerExecutor.class);

    public static BalloonToken execute(List<BalloonToken> inTokens, List<TransitionHandler> callbacks){

        List<BalloonToken> outTokens = new ArrayList<>(inTokens);
        List<TransitionHandler> callbacksSorted = new ArrayList<>(callbacks);

        callbacksSorted.sort(Comparator.comparingInt(TransitionHandler::getPriority));

        for(int i = 0; i < callbacksSorted.size(); i++){
            if(i < callbacksSorted.size() - 1){
                outTokens = callbacksSorted.get(i).processToken(outTokens);
            } else {
                BalloonToken tb = callbacksSorted.get(i).processToken(outTokens).get(0);
                outTokens = new ArrayList<>();
                outTokens.add(tb);
            }
        }

        if(outTokens.size() != 1){
            logger.error("[TransitionHandlerExecutor] Error! Wrong output token size.");
        }

        logger.info("[TransitionHandlerExecutor] Created new output token: " + outTokens.get(0));

        return outTokens.get(0);
    }
}
