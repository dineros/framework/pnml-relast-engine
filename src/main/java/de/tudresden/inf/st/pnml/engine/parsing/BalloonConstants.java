package de.tudresden.inf.st.pnml.engine.parsing;

public abstract class BalloonConstants {

    public static final String BALLOON_MARKING = "balloonMarking";
    public static final String BALLOON_PLACE_ID = "placeId";
    public static final String BALLOON_TOKENS = "tokens";
    public static final String BALLOON_TOKEN = "token";
}
