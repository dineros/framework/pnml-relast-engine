package de.tudresden.inf.st.pnml.engine.ros;

import de.tudresden.inf.st.pnml.jastadd.model.*;

import java.util.List;
import java.util.Set;

public class DiNeRosDefaultNode extends DiNeRosNode{


    public DiNeRosDefaultNode(String nodeName, PetriNet petriNet, String rcHost, String gcProtocol) {
        super(nodeName, petriNet, rcHost, gcProtocol);
    }

    public DiNeRosDefaultNode(String nodeName, PetriNet petriNet, BalloonMarking marking,
                              BalloonCallbackStorage callbackStorage, InputSignalConnector inputSignalConnector) {
        super(nodeName, petriNet, marking, callbackStorage, inputSignalConnector);
    }

    @Override
    protected void nodeLoop() {}

    protected TransitionSelectionResult onChange(List<Transition> enabledTransitions){

        // printTransitions(enabledTransitions);

        if(enabledTransitions.size() == 0){
            return new FiringSelectionNone();
        }

        TransitionSelectionResult res = fireRandomEnabledTransition(enabledTransitions);
        System.out.println("Selected transition: " + res.asFiringSelectionSuccess().getTransition().getId());
        return res;
    }

    protected TransitionSelectionResult onStartupEnded(List<Transition> enabledTransitions){

        // printTransitions(enabledTransitions);

        if(enabledTransitions.size() == 0){
            return new FiringSelectionNone();
        }

        TransitionSelectionResult res = fireRandomEnabledTransition(enabledTransitions);
        System.out.println("Selected transition: " + res.asFiringSelectionSuccess().getTransition().getId());
        return res;
    }

    public static void printTransitions(List<Transition> list){
        for(Transition t : list){
            System.out.println("Enabled t: " + t.getId());
        }
    }
}
