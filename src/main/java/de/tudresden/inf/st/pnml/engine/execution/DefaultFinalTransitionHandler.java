package de.tudresden.inf.st.pnml.engine.execution;

import de.tudresden.inf.st.pnml.jastadd.model.BalloonToken;

import java.util.ArrayList;
import java.util.List;

public class DefaultFinalTransitionHandler extends TransitionHandler {

    public DefaultFinalTransitionHandler(int priority) {
        super(priority, null);
    }

    @Override
    public List<BalloonToken> processToken(List<BalloonToken> tokens) {

        System.out.println("Executing default callback on " + tokens.size() + " balloon tokens");
        List<BalloonToken> tl = new ArrayList<>();
        tl.add(tokens.get(0));
        return tl;
    }
}
