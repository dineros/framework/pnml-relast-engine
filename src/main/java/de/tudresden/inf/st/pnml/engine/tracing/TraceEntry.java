package de.tudresden.inf.st.pnml.engine.tracing;

public class TraceEntry {

    public String traceType;
    public String traceValue;

    public TraceEntry(String traceType, String traceValue) {
        this.traceType = traceType;
        this.traceValue = traceValue;
    }
}
