package de.tudresden.inf.st.pnml.engine.fake;

import org.ros.exception.RemoteException;
import org.ros.exception.RosRuntimeException;
import org.ros.exception.ServiceNotFoundException;
import org.ros.namespace.GraphName;
import org.ros.node.AbstractNodeMain;
import org.ros.node.ConnectedNode;
import org.ros.node.service.ServiceClient;
import org.ros.node.service.ServiceResponseListener;
import rosjava_srv.StringService;
import rosjava_srv.StringServiceRequest;
import rosjava_srv.StringServiceResponse;

public class FakeClientNode extends AbstractNodeMain {

    @Override
    public GraphName getDefaultNodeName() {
        return GraphName.of("rosjava_srv_test/client");
    }

    @Override
    public void onStart(final ConnectedNode connectedNode) {

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ServiceClient<StringServiceRequest, StringServiceResponse> serviceClient;
        try {
            serviceClient = connectedNode.newServiceClient("sampleService0", StringService._TYPE);
        } catch (ServiceNotFoundException e) {
            throw new RosRuntimeException(e);
        }

        final StringServiceRequest request = serviceClient.newMessage();
        //request.setInput("(1) Hello there!");
        request.setInput("{color:\"blue\",pickPlaceSuccess:false}");

        serviceClient.call(request, new ServiceResponseListener<>() {
            @Override
            public void onSuccess(StringServiceResponse response) {
                System.out.println("(1) The response is : " + response.getOutput());
            }

            @Override
            public void onFailure(RemoteException e) {
                throw new RosRuntimeException(e);
            }
        });

        /*
        final StringServiceRequest request2 = serviceClient.newMessage();
        request2.setInput("(2) Hello there!");

        serviceClient.call(request2, new ServiceResponseListener<StringServiceResponse>() {
            @Override
            public void onSuccess(StringServiceResponse response) {
                System.out.println(String.format("(2) The response is : " + response.getOutput()));
            }

            @Override
            public void onFailure(RemoteException e) {
                throw new RosRuntimeException(e);
            }
        });
         */
    }
}