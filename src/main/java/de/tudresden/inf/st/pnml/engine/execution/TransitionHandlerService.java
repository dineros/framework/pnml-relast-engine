package de.tudresden.inf.st.pnml.engine.execution;

import de.tudresden.inf.st.pnml.jastadd.model.*;
import fr.lip6.move.pnml.ptnet.hlapi.TransitionHLAPI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class TransitionHandlerService {

    public static TransitionHandlerService INSTANCE = null;

    private Map<String, BalloonCallbackStorage> balloonCallbackStorages = new HashMap<>();

    private TransitionHandlerService() {}

    public static TransitionHandlerService getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new TransitionHandlerService();
        }

        return INSTANCE;
    }

    public void init(String petriNetId, BalloonCallbackStorage balloonCallbackStorage) {

        balloonCallbackStorages.put(petriNetId, balloonCallbackStorage);
    }

    public void registerHandler(PetriNet petriNet, String transitionId, int priority,
                                Function<List<Map<String, Object>>, List<Map<String, Object>>> processTokenFunction){

        TransitionHandler transitionHandler = new TransitionHandler(priority, processTokenFunction);

        if(balloonCallbackStorages.get(petriNet.getId()) == null){
            return;
        }

        List<TransitionHandler> transitionHandlers = new ArrayList<>();
        transitionHandlers.add(transitionHandler);
        BalloonTransition balloonTransition = balloonCallbackStorages.get(petriNet.getId()).resolveBalloonTransition(petriNet.getTransitionById(transitionId));
        balloonTransition.getTransitionHandlers().add(transitionHandler);
    }
}
