package de.tudresden.inf.st.pnml.engine.transform;

import de.tudresden.inf.st.pnml.engine.event.DiNeRosEvent;
import de.tudresden.inf.st.pnml.engine.event.DiNeRosEventTypes;
import de.tudresden.inf.st.pnml.engine.ros.DiNeRosNode;
import de.tudresden.inf.st.pnml.jastadd.model.*;

import java.io.IOException;

public class PetriNetInitializer {

    public static void initMutableTransitionInformation(String suffix, Transition t, DinerosTransition target) {

        DinerosTransition dt = t.asDinerosTransition();

        String usedSuffix = "";

        if(suffix != null){
            usedSuffix = "-" + suffix;
        }

        if (t.asDinerosTransition().getStaticTransitionInformation().isSignalTransitionInformation()) {
            SignalTransitionInformation ti = new SignalTransitionInformation();
            ti.setSubNet(dt.getStaticTransitionInformation().getSubNet() + usedSuffix);
            ti.setNode(dt.getStaticTransitionInformation().getNode());
            //ti.setClause(dt.getStaticTransitionInformation().asSignalTransitionInformation().getClause().deepCopy());
            target.setMutableTransitionInformation(ti);
        } else if (dt.getStaticTransitionInformation().isTopicTransitionInformation()) {
            deepCopyTopicTransitionInformation(target, dt, usedSuffix);
        } else if (dt.getStaticTransitionInformation().isServiceTransitionInformation()) {
            deepCopyServiceTransitionInformation(target, dt, usedSuffix);
        }
    }

    private static void deepCopyServiceTransitionInformation(DinerosTransition target, DinerosTransition dt, String usedSuffix) {
        ServiceTransitionInformation sri = new ServiceTransitionInformation();
        sri.setNode(dt.getStaticTransitionInformation().getNode());
        sri.setSubNet(dt.getStaticTransitionInformation().getSubNet() + usedSuffix);
        sri.setServiceName(dt.getStaticTransitionInformation().asServiceTransitionInformation().getServiceName());

        ServiceChannel sc = new ServiceChannel();
        sc.setResponsePlaceId(dt.getStaticTransitionInformation().asServiceTransitionInformation().getServerChannel().getResponsePlaceId());
        sc.setRequestPlaceId(dt.getStaticTransitionInformation().asServiceTransitionInformation().getServerChannel().getRequestPlaceId());
        sc.setId(sc.getId());
        sri.setServerChannel(sc);

        for(ServiceChannel sci : dt.getStaticTransitionInformation().asServiceTransitionInformation().getClientChannelList()){
            ServiceChannel newClientChannel = new ServiceChannel();
            newClientChannel.setId(sci.getId());
            newClientChannel.setResponsePlaceId(sci.getResponsePlaceId());
            newClientChannel.setRequestPlaceId(sci.getRequestPlaceId());
            sri.addClientChannel(newClientChannel);
        }

        target.setMutableTransitionInformation(sri);
    }

    private static void deepCopyTopicTransitionInformation(DinerosTransition target, DinerosTransition dt, String usedSuffix) {
        TopicTransitionInformation tri = new TopicTransitionInformation();
        tri.setNode(dt.getStaticTransitionInformation().getNode());
        tri.setSubNet(dt.getStaticTransitionInformation().getSubNet() + usedSuffix);
        tri.setTopic(dt.getStaticTransitionInformation().asTopicTransitionInformation().getTopic());

        for(SubscriberPort sp : dt.getStaticTransitionInformation().asTopicTransitionInformation().getSubscriberPortList()){
            SubscriberPort newSp = new SubscriberPort();
            newSp.setLimit(sp.getLimit());
            newSp.setPlaceId(sp.getPlaceId());
            tri.getSubscriberPortList().add(newSp);
        }

        for(PublisherPort pp : dt.getStaticTransitionInformation().asTopicTransitionInformation().getPublisherPortList()){
            PublisherPort newPp = new PublisherPort();
            newPp.setLimit(pp.getLimit());
            newPp.setPlaceId(pp.getPlaceId());
            tri.getPublisherPortList().add(newPp);
        }

        target.setMutableTransitionInformation(tri);
    }

    private static boolean signalIsUsed(PetriNet petriNet, String signalId){
        for(DinerosTransition t : petriNet.allDinerosTransitions()){
            if(t.getStaticTransitionInformation().
                    asSignalTransitionInformation().getClause().hasLiteral(signalId)){
                return true;
            }
        }
        return false;
    }

    public static void initInputSignalConnections(PetriNet petriNet, String host, String protocol,
                                                  InputSignalConnector inputSignalConnector, DiNeRosNode node) throws IOException {

        for(String signalId : petriNet.getInputSignalDefinition().keySet()){
            if(signalIsUsed(petriNet, signalId)) {
                // mqtt connections
                SignalConnection sc = new SignalConnection();
                sc.setCurrentValue(Boolean.parseBoolean(petriNet.getInputSignalDefinition().get(signalId)));
                sc.setId(signalId);
                inputSignalConnector.addSignalConnection(sc);
                sc.connectCurrentValue(protocol + "://" + host + "/" + signalId);

                // internal java connections
                sc.connectCurrentValue("java://localhost:00/" + signalId, false);
                //inputSignalConnector.ragconnectJavaRegisterConsumer("java://localhost:00/" + signalId,
                //  bytes -> node.notify(DiNeRosNode.NOTIFICATION_SIGNAL_CHANGE));

                inputSignalConnector.ragconnectJavaRegisterConsumer(signalId, bytes -> notifyOnSignalChange(node, signalId));
            } else {
                System.out.println("Skipping unused signal: " + signalId);
            }
        }
    }

    public static void notifyOnSignalChange(DiNeRosNode node, String signalId){
        System.out.println("NOTIFY ON SIGNAL CHANGE");
        node.notify(new DiNeRosEvent(DiNeRosEventTypes.NOTIFICATION_SIGNAL_CHANGE, signalId));
    }

    public void init(PetriNet petriNet, String host, String protocol, InputSignalConnector isc, DiNeRosNode node){

        for(Transition t : petriNet.allTransitions()){
            initMutableTransitionInformation(null, t, t.asDinerosTransition());
        }

        try {
            initInputSignalConnections(petriNet, host, protocol, isc, node);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}