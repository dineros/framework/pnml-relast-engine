package de.tudresden.inf.st.pnml.engine.execution;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import de.tudresden.inf.st.pnml.jastadd.model.BalloonToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;

public class TransitionHandler {

    protected static final Logger logger = LoggerFactory.getLogger(TransitionHandler.class);
    private final String id;
    private final int priority;

    protected Function<List<Map<String, Object>>, List<Map<String, Object>>> processTokenFunction;

    public TransitionHandler(int priority, Function<List<Map<String, Object>>, List<Map<String, Object>>> processTokenFunction) {
        this.id = UUID.randomUUID().toString();
        this.priority = priority;
        this.processTokenFunction = processTokenFunction;
    }

    public String getId() {
        return id;
    }

    public int getPriority() {
        return priority;
    }

    private Map<String, Object> parseToken(String s) {

        Gson gson = new GsonBuilder().create();
        Type mapType = new TypeToken<Map<String, Object>>(){}.getType();
        return gson.fromJson(s, mapType);
    }

    public List<BalloonToken> processToken(List<BalloonToken> tokens) {

        List<Map<String, Object>> parsedTokens = new ArrayList<>();
        List<BalloonToken> resultBalloonTokens = new ArrayList<>();

        for (BalloonToken bt : tokens) {
            Map<String, Object> t = parseToken(bt.getValue());
            parsedTokens.add(t);
        }

        List<Map<String, Object>> resultTokens = processTokenFunction.apply(parsedTokens);

        for (Map<String, Object> t : resultTokens) {
            BalloonToken newBt = new BalloonToken();
            Gson gson = new GsonBuilder().create();
            newBt.setValue(gson.toJson(t));
            resultBalloonTokens.add(newBt);
        }

        return resultBalloonTokens;
    }
}
