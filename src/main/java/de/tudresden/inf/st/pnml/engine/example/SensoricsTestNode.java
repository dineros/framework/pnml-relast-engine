package de.tudresden.inf.st.pnml.engine.example;

import de.tudresden.inf.st.pnml.engine.ros.DiNeRosNode;
import de.tudresden.inf.st.pnml.jastadd.model.*;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Random;

public class SensoricsTestNode extends DiNeRosNode {

    public SensoricsTestNode(String nodeName, PetriNet petriNet, String rcHost, String gcProtocol) {
        super(nodeName, petriNet, rcHost, gcProtocol);
    }

    @Override
    protected void nodeLoop() {}

    @Override
    protected TransitionSelectionResult onChange(List<Transition> enabledTransitions) {
        return enabledTransitions.size() == 0 ?
                new FiringSelectionNone() : fireRandomEnabledTransition(enabledTransitions);
    }

    @Override
    protected TransitionSelectionResult onStartupEnded(List<Transition> enabledTransitions) {
        return new FiringSelectionNone();
    }
}
