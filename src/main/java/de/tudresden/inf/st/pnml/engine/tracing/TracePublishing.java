package de.tudresden.inf.st.pnml.engine.tracing;

import de.tudresden.inf.st.pnml.engine.ros.RosCommunicationUtil;
import org.ros.node.ConnectedNode;
import org.ros.node.topic.Publisher;

public class TracePublishing {

    public static java.lang.String TraceTypeTransitionFired = "transitionFired";
    public static java.lang.String TraceTypeSignalChanged = "signalChanged";
    public static java.lang.String TraceTypeTopicSend = "topicSend";
    public static java.lang.String TraceTypeTopicReceive = "topicReceive";
    public static java.lang.String TraceTypeServiceClientSend = "serviceClientSend";
    public static java.lang.String TraceTypeServiceClientReceive = "serviceClientReceive";
    public static java.lang.String TraceTypeServiceServerSend = "serviceServerSend";
    public static java.lang.String TraceTypeServiceServerReceive = "serviceServerReceive";

    public static void publish(java.lang.String traceType, java.lang.String traceValue, ConnectedNode connectedNode,
                               Publisher<std_msgs.String> publisherObj) {
        java.lang.String msgString = traceType + "," + traceValue;
        RosCommunicationUtil.publish("DinerosTracing", msgString.getBytes(), connectedNode, publisherObj);
    }
}
