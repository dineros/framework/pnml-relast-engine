package de.tudresden.inf.st.pnml.engine.example;

import de.tudresden.inf.st.pnml.engine.ros.DiNeRosNode;
import de.tudresden.inf.st.pnml.jastadd.model.*;
import org.jetbrains.annotations.NotNull;
import org.ros.concurrent.CancellableLoop;

import java.util.List;
import java.util.Set;

public class DinerosTestNode extends DiNeRosNode {

    boolean execOnce = false;

    public DinerosTestNode(String nodeName, PetriNet petriNet) {
        super(nodeName, petriNet, "localhost", "mqtt");
    }

    public DinerosTestNode(String nodeName, PetriNet petriNet, BalloonMarking marking,
                           BalloonCallbackStorage callbackStorage) {
        super(nodeName, petriNet, "localhost", "mqtt");
        this.marking = marking;
        this.callbackStorage = callbackStorage;
    }

    @Override
    protected void nodeLoop() {

        this.connectedNode.executeCancellableLoop(new CancellableLoop() {

            @Override
            protected void loop() throws InterruptedException {

                Thread.sleep(1000);

                if(!execOnce) {
                    execOnce = true;
                }
            }
        });
    }

    @Override
    protected TransitionSelectionResult onChange(List<Transition> enabledTransitions) {
        System.out.println("Calling onWaitEnded");
        return fireRandomEnabledTransition(enabledTransitions);
    }

    @Override
    protected TransitionSelectionResult onStartupEnded(List<Transition> enabledTransitions) {
        System.out.println("Calling onStartupEnded");
        return fireRandomEnabledTransition(enabledTransitions);
    }
}
