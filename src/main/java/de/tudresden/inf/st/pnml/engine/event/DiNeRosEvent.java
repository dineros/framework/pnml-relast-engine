package de.tudresden.inf.st.pnml.engine.event;

public class DiNeRosEvent {

    public String eventType;
    public String payload;

    public DiNeRosEvent(String eventType) {
        this(eventType, null);
    }

    public DiNeRosEvent(String eventType, String payload) {
        if(eventType.equals(DiNeRosEventTypes.NOTIFICATION_MARKING_CHANGE) || eventType.equals(DiNeRosEventTypes.NOTIFICATION_SIGNAL_CHANGE) ||
                eventType.equals(DiNeRosEventTypes.NOTIFICATION_WAIT_ENDED) || eventType.equals(DiNeRosEventTypes.NOTIFICATION_STARTUP_ENDED) ||
                eventType.equals(DiNeRosEventTypes.NOTIFICATION_SERVICE_REQ_CLIENT) || eventType.equals(DiNeRosEventTypes.NOTIFICATION_TOPIC_PUB) ||
                eventType.equals(DiNeRosEventTypes.NOTIFICATION_SERVICE_RES_SERVER)) {
            this.eventType = eventType;
        } else {
            throw new RuntimeException("Invalid event type used!");
        }
        this.payload = payload;
    }
}
