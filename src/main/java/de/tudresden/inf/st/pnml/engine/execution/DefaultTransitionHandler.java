package de.tudresden.inf.st.pnml.engine.execution;

import de.tudresden.inf.st.pnml.jastadd.model.BalloonToken;

import java.util.List;

public class DefaultTransitionHandler extends TransitionHandler {

    public DefaultTransitionHandler(int priority) {
        super(priority, null);
    }

    @Override
    public List<BalloonToken> processToken(List<BalloonToken> tokens) {
        return tokens;
    }
}
