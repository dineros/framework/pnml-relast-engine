package de.tudresden.inf.st.pnml.engine;

import de.tudresden.inf.st.pnml.engine.fake.FakeClientNode;
import de.tudresden.inf.st.pnml.engine.fake.FakeServerNode;
import org.ros.node.DefaultNodeMainExecutor;
import org.ros.node.NodeConfiguration;
import org.ros.node.NodeMainExecutor;

import java.net.URI;

public class Main {

    private static final String ROS_HOST = "localhost";
    private static final String ROS_MASTER_URI = "http://localhost:11311";

    private static final NodeConfiguration nodeConfiguration = NodeConfiguration.newPublic(ROS_HOST);
    private static final NodeMainExecutor nodeMainExecutor = DefaultNodeMainExecutor.newDefault();

    public static void main(java.lang.String[] args) {

        //Server server = new Server();
        // FakeClientNode client = new FakeClientNode();
        // nodeConfiguration.setMasterUri(URI.create(ROS_MASTER_URI));

        //System.out.println("---- STARTING SERVER ----");
        //new Thread(() -> nodeMainExecutor.execute(server, nodeConfiguration)) {{start();}};


     /*   System.out.println("---- STARTING sampleService0 ----");
        new Thread(() -> nodeMainExecutor.execute(new FakeServerNode("sampleService0", "sampleServiceNode0"), nodeConfiguration)) {{start();}};

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

       // System.out.println("---- STARTING CLIENT ----");
      //  new Thread(() -> nodeMainExecutor.execute(client, nodeConfiguration)) {{start();}};

        System.out.println("---- STARTING sampleService1 ----");
        new Thread(() -> nodeMainExecutor.execute(new FakeServerNode("sampleService1", "sampleServiceNode1"), nodeConfiguration)) {{start();}};*/
    }
}
