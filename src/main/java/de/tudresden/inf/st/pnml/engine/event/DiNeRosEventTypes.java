package de.tudresden.inf.st.pnml.engine.event;

public class DiNeRosEventTypes {

    public static final String NOTIFICATION_MARKING_CHANGE = "markingChange";
    public static final String NOTIFICATION_SIGNAL_CHANGE = "signalChange";
    public static final String NOTIFICATION_WAIT_ENDED = "waitEnded";
    public static final String NOTIFICATION_STARTUP_ENDED = "startEnded";
    public static final String NOTIFICATION_SERVICE_REQ_CLIENT = "serviceReq";
    public static final String NOTIFICATION_SERVICE_RES_SERVER = "serviceRes";
    public static final String NOTIFICATION_TOPIC_PUB = "topicPub";
}
