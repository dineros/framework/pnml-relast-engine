package de.tudresden.inf.st.pnml.engine.ros;

import org.ros.node.ConnectedNode;
import org.ros.node.topic.Publisher;

import java.nio.charset.StandardCharsets;

public class RosCommunicationUtil {

    public static boolean publish(java.lang.String topic, byte[] msgContent, ConnectedNode node, Publisher<std_msgs.String> pub){

        System.out.println("Publishing new message to " + topic);

        if(pub == null){
            pub = node.newPublisher(topic, std_msgs.String._TYPE);
        }

        if(pub != null){
            while(true){
                if(node != null){
                    break;
                }
            }

            std_msgs.String msg = pub.newMessage();
            java.lang.String s = new java.lang.String(msgContent, StandardCharsets.UTF_8);
            msg.setData(s);
            pub.publish(msg);

            return true;
        }
        return false;
    }
}
