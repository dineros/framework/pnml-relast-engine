package de.tudresden.inf.st.pnml.engine.transform;

import de.tudresden.inf.st.pnml.jastadd.model.*;

import java.util.*;

public class ServiceInstanceUtil {

    public static void updateInstanceIds(Page page, String suffix) {

        for (PnObject pnObject : page.getObjects()) {
            pnObject.setId(pnObject.getId() + suffix);

            if (pnObject.hasName()) {
                Name name = new Name();
                name.setText(pnObject.getId());
                pnObject.setName(name);
            } else {
                pnObject.getName().setText(pnObject.getId());
            }

            if (pnObject.isPageNode()) {
                updateInstanceIds(pnObject.asPage(), suffix);
            }
        }
    }

    public static void reintegrateArcs(Page original, Page copy, String suffix) {

        Set<Arc> originalArcs = new HashSet<>();
        getArcsInPage(original, originalArcs);

        for (Arc a : originalArcs) {

            Page originalArcPage = Objects.requireNonNull(getObjectInPage(original, a.getId()))._2;
            Page arcPage = Objects.requireNonNull(getObjectInPage(copy,
                    originalArcPage.getId() + suffix))._1.asPage();
            Pair<PnObject, Page> source = getObjectInPage(copy, a.getSource().getId() + suffix);
            Pair<PnObject, Page> target = getObjectInPage(copy, a.getTarget().getId() + suffix);
            Arc copyArc = new Arc();
            assert source != null;
            copyArc.setSource(source._1.asNode());
            assert target != null;
            copyArc.setTarget(target._1.asNode());
            copyArc.setId(a.getId() + suffix);
            Name n = new Name();
            n.setText(a.getName().getText() + suffix);
            copyArc.setName(n);
            arcPage.addObject(copyArc);

            System.out.println(
                    "Created arc: " + copyArc.getId()
                            + " from " + copyArc.getSource().getId()
                            + " to " + copyArc.getTarget().getId()
                            + " in page " + arcPage.getId());
        }
    }

    private static Pair<PnObject, Page> getObjectInPage(Page page, String id) {

        if (page.getId().equals(id)) {
            return new Pair<>(page, page);
        }

        for (PnObject o : page.getObjects()) {
            if (o.getId().equals(id)) {
                return new Pair<>(o, page);
            }
            if (o.isPageNode()) {
                getObjectInPage(o.asPage(), id);
            }
        }

        return null;
    }

    private static void getArcsInPage(Page page, Set<Arc> arcs) {

        for (PnObject o : page.getObjects()) {
            if (o.isArcNode()) {
                arcs.add(o.asArc());
            }
            if (o.isPageNode()) {
                getArcsInPage(o.asPage(), arcs);
            }
        }
    }

    public static Page treeCopyWithoutArcs(PetriNet petriNet, Page currentPage) {
        return treeCopyWithoutArcsInternal(petriNet, currentPage, null);
    }

    private static Page treeCopyWithoutArcsInternal(PetriNet petriNet, Page currentPage, Page parent) {

        Map<String, PnObject> rMap = new HashMap<>();

        if (parent == null) {
            parent = new Page();
            parent.setId(currentPage.getId());
            parent.setToolspecificList(currentPage.getToolspecificList().treeCopy());
            parent.setName(currentPage.getName().treeCopy());
        }

        for (PnObject pnObject : currentPage.getObjects()) {
            if (pnObject.isPageNode()) {
                Page copyPage = new Page();
                copyPage.setId(pnObject.asPage().getId());
                copyPage.setToolspecificList(pnObject.asPage().getToolspecificList().treeCopy());
                copyPage.setName(pnObject.getName().treeCopy());
                parent.addObject(copyPage);

                treeCopyWithoutArcsInternal(petriNet, pnObject.asPage(), copyPage);
            }

            if (pnObject.isPlaceObject()) {
                DinerosPlace copyP = new DinerosPlace();
                copyP.setId(pnObject.getId());
                copyP.setName(pnObject.getName().treeCopy());
                copyP.setInitialMarking(pnObject.asDinerosPlace().getInitialMarking().treeCopy());
                copyP.setToolspecificList(pnObject.getToolspecificList().treeCopy());
                parent.addObject(copyP);
                rMap.put(copyP.getId(), copyP);
            }

            if (pnObject.isTransitionObject()) {
                DinerosTransition copyT = new DinerosTransition();
                copyT.setId(pnObject.getId());
                copyT.setName(pnObject.getName().treeCopy());
                copyT.setToolspecificList(pnObject.getToolspecificList().treeCopy());
                parent.addObject(copyT);
                rMap.put(copyT.getId(), copyT);
            }
        }

        for (PnObject pnObject : currentPage.getObjects()) {

            Name n = new Name();
            n.setText(pnObject.getId());

            if (pnObject.isRefTransitionObject()) {
                RefTransition copyRt = new RefTransition();
                copyRt.setId(pnObject.getId());
                copyRt.setName(n.treeCopy());
                copyRt.setRef(Objects.requireNonNull(getReferencedObject(petriNet,
                        rMap, pnObject.asRefTransitionObject().getRef().getId())).asDinerosTransition());
                copyRt.setToolspecificList(pnObject.getToolspecificList().treeCopy());
                parent.addObject(copyRt);
            }

            if (pnObject.isRefPlaceObject()) {
                RefPlace copyRp = new RefPlace();
                copyRp.setId(pnObject.getId());
                copyRp.setName(n.treeCopy());
                copyRp.setRef(Objects.requireNonNull(getReferencedObject(petriNet,
                        rMap, pnObject.asRefPlaceObject().getRef().getId())).asDinerosPlace());
                copyRp.setToolspecificList(pnObject.getToolspecificList().treeCopy());
                parent.addObject(copyRp);
            }
        }

        return parent;
    }

    private static PnObject getReferencedObject(PetriNet petriNet,  Map<String, PnObject> rMap, String id){

        PnObject referencedObject = rMap.get(id);

        if(referencedObject == null){
            System.out.println("Searching for external referenced object: " + id);
            for(PnObject pnObject : petriNet.allObjects()){
                if(pnObject.getId().equals(id)){
                    System.out.println("Found external referenced object: " + id);
                    return pnObject;
                }
            }
        } else {
            return referencedObject;
        }

        return null;
    }
}