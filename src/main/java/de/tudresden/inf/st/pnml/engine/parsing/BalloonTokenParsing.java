package de.tudresden.inf.st.pnml.engine.parsing;

import de.tudresden.inf.st.pnml.jastadd.model.Place;
import de.tudresden.inf.st.pnml.jastadd.model.ToolInfo;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class BalloonTokenParsing {

    public static List<String> parseBalloonPlaceMarkingToolSpecifics(Place place) throws ParserConfigurationException, IOException, SAXException {

        List<String> marking = new ArrayList<>();

        for (ToolInfo ti : place.getToolspecifics()) {
            if (ti.getFormattedXMLBuffer().lastIndexOf(BalloonConstants.BALLOON_MARKING) != -1) {

                StringBuffer toolInfoStringBuffer = ti.getFormattedXMLBuffer();

                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

                Document doc = dBuilder.parse(fromStringBuffer(toolInfoStringBuffer));
                doc.getDocumentElement().normalize();

                org.w3c.dom.NodeList tokensList = doc.getElementsByTagName(BalloonConstants.BALLOON_TOKENS);

                for (int i = 0; i < tokensList.getLength(); i++) {

                    for (int j = 0; j < tokensList.item(i).getChildNodes().getLength(); j++)

                        if (tokensList.item(i).getChildNodes().item(j).getNodeName().equals(BalloonConstants.BALLOON_TOKEN)) {
                            marking.add(tokensList.item(i).getChildNodes().item(j).getTextContent());
                        }
                }
                break;
            }
        }
        return marking;
    }

    private static InputStream fromStringBuffer(StringBuffer buf) {
        return new ByteArrayInputStream(buf.toString().getBytes());
    }
}
