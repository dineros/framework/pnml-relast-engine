package de.tudresden.inf.st.pnml.engine.ros;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import de.tudresden.inf.st.pnml.base.constants.PnmlConstants;
import de.tudresden.inf.st.pnml.base.data.ClauseValuesDefinition;
import de.tudresden.inf.st.pnml.engine.event.DiNeRosEvent;
import de.tudresden.inf.st.pnml.engine.event.DiNeRosEventTypes;
import de.tudresden.inf.st.pnml.engine.execution.TransitionHandler;
import de.tudresden.inf.st.pnml.engine.tracing.TracePublishing;
import de.tudresden.inf.st.pnml.engine.transform.PetriNetInitializer;
import de.tudresden.inf.st.pnml.engine.transform.ServiceInstanceUtil;
import de.tudresden.inf.st.pnml.jastadd.model.*;
import org.jetbrains.annotations.NotNull;
import org.ros.concurrent.CancellableLoop;
import org.ros.exception.RemoteException;
import org.ros.exception.ServiceNotFoundException;
import org.ros.namespace.GraphName;
import org.ros.node.AbstractNodeMain;
import org.ros.node.ConnectedNode;
import org.ros.node.service.ServiceClient;
import org.ros.node.service.ServiceResponseBuilder;
import org.ros.node.service.ServiceResponseListener;
import org.ros.node.topic.Publisher;
import org.ros.node.topic.Subscriber;
import org.xml.sax.SAXException;
import rosjava_srv.StringService;
import rosjava_srv.StringServiceRequest;
import rosjava_srv.StringServiceResponse;
import std_msgs.String;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.*;
import java.util.function.Function;

public abstract class DiNeRosNode extends AbstractNodeMain {

    public final java.lang.String nodeName;
    public final PetriNet petriNet;
    protected BalloonMarking marking;
    public BalloonCallbackStorage callbackStorage;
    public ConnectedNode connectedNode;
    protected InputSignalConnector inputSignalConnector;
    private boolean stopNode = false;

    private final Map<DinerosPlace, Publisher<String>> dinerosPublishers = new HashMap<>();
    private final Set<java.lang.String> responsePlaces = new HashSet<>();
    private final Set<java.lang.String> activeServerInstances = new HashSet<>();
    public Publisher<String> tracePublisher = null;

    public DiNeRosNode(java.lang.String nodeName, PetriNet petriNet, java.lang.String rcHost, java.lang.String gcProtocol) {
        this.nodeName = nodeName;
        this.petriNet = petriNet;

        try {
            marking = petriNet.initializeBalloonMarking();
            callbackStorage = petriNet.initializeCallbackStorage();
            inputSignalConnector = new InputSignalConnector();
            PetriNetInitializer.initInputSignalConnections(petriNet, rcHost, gcProtocol, inputSignalConnector, this);
        } catch (IOException | SAXException | ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    public void registerHandler(java.lang.String transitionId, int priority,
                                Function<List<Map<java.lang.String, Object>>,
                                        List<Map<java.lang.String, Object>>> processTokenFunction) {

        TransitionHandler transitionHandler = new TransitionHandler(priority, processTokenFunction);
        integrateHandler(transitionId, transitionHandler);
    }

    public void registerHandler(java.lang.String transitionId, TransitionHandler transitionHandler) {

        integrateHandler(transitionId, transitionHandler);
    }

    private void integrateHandler(java.lang.String transitionId, TransitionHandler transitionHandler) {
        if (petriNet.getTransitionById(transitionId) == null) {
            return;
        }

        BalloonTransition balloonTransition = callbackStorage.resolveBalloonTransition(petriNet.getTransitionById(transitionId));

        if (balloonTransition == null) {
            return;
        }

        balloonTransition.getTransitionHandlers().add(transitionHandler);
    }

    public DiNeRosNode(java.lang.String nodeName, PetriNet petriNet, BalloonMarking marking,
                       BalloonCallbackStorage callbackStorage, InputSignalConnector inputSignalConnector) {
        this.nodeName = nodeName;
        this.petriNet = petriNet;
        this.marking = marking;
        this.callbackStorage = callbackStorage;
        this.inputSignalConnector = inputSignalConnector;
    }

    private void internalNodeLoop() {

        this.connectedNode.executeCancellableLoop(new CancellableLoop() {

            @Override
            protected void loop() {
                if (!stopNode) {
                    nodeLoop();
                }
            }
        });
    }

    protected void nodeLoop() {}

    protected final void stop() {
        stopNode = true;
        this.connectedNode.shutdown();
    }

    public synchronized void notify(DiNeRosEvent event) {

        List<Transition> signalFilteredTransitions = getSignalFilteredTransitions();

        switch (event.eventType) {
            case DiNeRosEventTypes.NOTIFICATION_MARKING_CHANGE:
                System.out.println("NOTIFICATION_MARKING_CHANGE");
                onChangeInternal(signalFilteredTransitions);
                break;
            case DiNeRosEventTypes.NOTIFICATION_SIGNAL_CHANGE:
                System.out.println("NOTIFICATION_SIGNAL_CHANGE");
                handleSignalChange(event);
                onChangeInternal(signalFilteredTransitions);
                break;
            case DiNeRosEventTypes.NOTIFICATION_STARTUP_ENDED:
                System.out.println("NOTIFICATION_STARTUP_ENDED");
                onStartupEndedInternal(signalFilteredTransitions);
                break;
            case DiNeRosEventTypes.NOTIFICATION_SERVICE_REQ_CLIENT:
                System.out.println("NOTIFICATION_SERVICE_REQ_CLIENT");
                onServiceRequestAvailableOnClientSide(event.payload);
                break;
            case DiNeRosEventTypes.NOTIFICATION_TOPIC_PUB:
                System.out.println("NOTIFICATION_TOPIC_PUB");
                onTopicPublisherAvailable(event.payload);
                break;
            case DiNeRosEventTypes.NOTIFICATION_SERVICE_RES_SERVER:
                System.out.println("NOTIFICATION_SERVICE_RES_SERVER");
                onServiceResponseAvailableOnServerSide(event.payload);
                break;
        }
    }

    private void handleSignalChange(DiNeRosEvent event) {
        Boolean newVal = false;

        for(SignalConnection sc : this.inputSignalConnector.getSignalConnections()){
            if(sc.getId().equals(event.payload)){
                newVal = sc.getCurrentValue();
            }
        }

        TracePublishing.publish(TracePublishing.TraceTypeSignalChanged,
                this.getDefaultNodeName()+ " " + event.payload + " " + newVal,
                this.connectedNode, this.tracePublisher);
    }

    private void onTopicPublisherAvailable(java.lang.String placeId) {

        java.lang.String[] split = placeId.split("-INSTANCE-");
        java.lang.String originalId = split[0];
        java.lang.String topicName = petriNet.getPortNameByPlaceId(originalId, PnmlConstants.CHANNEL_PLACE_TYPE_PUB_KEY);
        Place place = petriNet.getPlaceById(placeId);
        BalloonMarkedPlace bmp = this.marking.resolveBalloonPlace(place);
        TracePublishing.publish(TracePublishing.TraceTypeTopicSend,
                this.nodeName + " " + topicName + " " + placeId, this.connectedNode, this.tracePublisher);
        RosCommunicationUtil.publish(topicName, bmp.getBalloonMarking(0).getValue().getBytes(),
                this.connectedNode, getPublisherByPlaceId(placeId));
        System.out.println("[DiNeROS-Node] [" + nodeName + "] Published msg: " + bmp.getBalloonMarking(0).getValue());
        bmp.getBalloonMarking(0).removeSelf();
        petriNet.flushTreeCache();
        this.notify(new DiNeRosEvent(DiNeRosEventTypes.NOTIFICATION_MARKING_CHANGE));

    }

    private void onServiceResponseAvailableOnServerSide(java.lang.String placeId) {

        System.out.println("Active instances:");
        for(java.lang.String s : activeServerInstances){
            System.out.println("i: " + s);
        }

        for (java.lang.String entry : responsePlaces) {
            if (entry.equals(placeId)) {
                System.out.println("Removing from active instance list: " + entry);
                activeServerInstances.remove(entry);
                return;
            }
        }

        System.err.println("[DiNeROS-Node] [" + nodeName + "] Tried to remove non active instance: " + placeId);
    }

    private void onServiceRequestAvailableOnClientSide(java.lang.String placeId) {

        ServiceClient<StringServiceRequest, StringServiceResponse> serviceClient;

        // crop name if place is within a server instance
        // Naming pattern is here: original PlaceID + "-INSTANCE-" + instanceId
        java.lang.String[] split = placeId.split("-INSTANCE-");
        java.lang.String originalId = split[0];


        java.lang.String serviceName = petriNet.getPortNameByPlaceId(originalId, PnmlConstants.CHANNEL_PLACE_TYPE_CLIENT_REQ_KEY);
        java.lang.String cResPlaceId;

        if(split.length > 1){
            cResPlaceId = petriNet.getServiceClientResponsePlaceId(originalId) + "-INSTANCE-" + placeId.split("-INSTANCE-")[split.length - 1];
        } else {
            cResPlaceId = petriNet.getServiceClientResponsePlaceId(originalId);
        }

        Place targetPlace = petriNet.getPlaceById(cResPlaceId);
        Place sourcePlace = petriNet.getPlaceById(placeId);
        BalloonMarkedPlace bmpSource = this.marking.resolveBalloonPlace(sourcePlace);
        BalloonMarkedPlace bmpTarget = this.marking.resolveBalloonPlace(targetPlace);

        DiNeRosNode self = this;

        try {
            System.out.println("[DiNeROS-Node] [" + nodeName + "] Creating client for service: " + serviceName);
            serviceClient = connectedNode.newServiceClient(serviceName, StringService._TYPE);
            final StringServiceRequest request = serviceClient.newMessage();
            request.setInput(bmpSource.getBalloonMarking(0).getValue());

            TracePublishing.publish(TracePublishing.TraceTypeServiceClientSend,
                    self.nodeName + " " + serviceName + "-" + placeId, self.connectedNode, self.tracePublisher);

            serviceClient.call(request, new ServiceResponseListener<>() {

                @Override
                public void onSuccess(StringServiceResponse stringServiceResponse) {
                    System.out.println("[DiNeROS-Node] [" + nodeName + "] Received response from service: " + serviceName);
                    System.out.println("MARKING BEFORE APP:" + marking.print());
                    bmpSource.getBalloonMarking(0).removeSelf();
                    bmpTarget.addBalloonMarking(new BalloonToken(stringServiceResponse.getOutput(), System.currentTimeMillis()));
                    marking.callServiceIfRequest(bmpTarget, self);

                    if(petriNet.getPortNameByPlaceId(cResPlaceId, PnmlConstants.CHANNEL_PLACE_TYPE_PUB_KEY) != null ){
                        System.out.println("[DiNeROS-Node] [" + nodeName + "] Orphan publisher place ready: " + cResPlaceId);
                        self.notify(new DiNeRosEvent(DiNeRosEventTypes.NOTIFICATION_TOPIC_PUB, cResPlaceId));
                    } else {
                        self.notify(new DiNeRosEvent(DiNeRosEventTypes.NOTIFICATION_MARKING_CHANGE));
                    }

                    TracePublishing.publish(TracePublishing.TraceTypeServiceClientReceive,
                            self.nodeName + " " + serviceName + " " + cResPlaceId, self.connectedNode, self.tracePublisher);

                    // serviceClient.shutdown();
                }

                @Override
                public void onFailure(RemoteException e) {
                    System.err.println("[DiNeROS-Node] [" + nodeName + "] Error while calling: token will not be removed!");
                    serviceClient.shutdown();
                }
            });

        } catch (ServiceNotFoundException e) {
            System.err.println("Error while calling service.");
        }
    }

    @NotNull
    protected List<Transition> getSignalFilteredTransitions() {

        List<Transition> signalFilteredTransitions = new ArrayList<>();
        ClauseValuesDefinition clauseValuesDefinition = new ClauseValuesDefinition();

        petriNet.flushAttrAndCollectionCache();
        petriNet.flushTreeCache();

       for (Transition transition : marking.enabledBalloonTransitions()) {
            if (transition.asDinerosTransition().getStaticTransitionInformation()
                    .asSignalTransitionInformation().getClause() != null) {
                for (java.lang.String signalId : transition.asDinerosTransition().getStaticTransitionInformation()
                        .asSignalTransitionInformation().getClause().signals()) {
                    for (SignalConnection sc : inputSignalConnector.getSignalConnections()) {
                        if (signalId.equals(sc.getId())) {
                            clauseValuesDefinition.addDef(sc.getId(), sc.getCurrentValue());
                            break;
                        }
                    }
                }

                if ((clauseValuesDefinition.getDefs().size() == 0) ||
                        (transition.asDinerosTransition().getStaticTransitionInformation().
                                asSignalTransitionInformation().getClause().evalClause(clauseValuesDefinition))) {

                    signalFilteredTransitions.add(transition);
                }
            }
        }

        for(Transition t : signalFilteredTransitions){
            System.out.println("Enabled: " + t.getId());
        }

        return signalFilteredTransitions;
    }

    private void getTransitionSelectionResult(TransitionSelectionResult res) {

        if (res.isFiringSelectionNone()) {
            return;
        }

        if (res.isFiringSelectionFail()) {
            System.err.println("[DiNeROS-Node] [" + nodeName + "] Firing selection action failed!");
            return;
        }

        System.out.println("[DiNeROS-Node] [" + nodeName + "] " +
                "Firing Transition " + res.asFiringSelectionSuccess().getTransition().getId());
        marking.fireTransition(res.asFiringSelectionSuccess()
                .getTransition(), callbackStorage, inputSignalConnector, this, true);

    }

    private void onStartupEndedInternal(List<Transition> enabledTransitions) {

        System.out.println("Calling onStartupEndedInternal");

        TransitionSelectionResult res = onStartupEnded(enabledTransitions);
        getTransitionSelectionResult(res);
    }

    private void onChangeInternal(List<Transition> enabledTransitions) {

        System.out.println("Calling onChangeInternal");

        TransitionSelectionResult res = onChange(enabledTransitions);

        if(res.isFiringSelectionSuccess()){
            TracePublishing.publish(TracePublishing.TraceTypeTransitionFired,
                    this.getDefaultNodeName() + " " + res.asFiringSelectionSuccess().getTransition().getId(),
                    this.connectedNode, this.tracePublisher);
        }

        getTransitionSelectionResult(res);
    }

    protected abstract TransitionSelectionResult onChange(List<Transition> enabledTransitions);

    protected abstract TransitionSelectionResult onStartupEnded(List<Transition> enabledTransitions);

    @Override
    public GraphName getDefaultNodeName() {
        return GraphName.of(nodeName);
    }

    @Override
    public void onStart(final ConnectedNode connectedNode) {

        DiNeRosNode self = this;
        this.connectedNode = connectedNode;

        System.out.println("[" + nodeName + "] Initializing node");
        System.out.println("[" + nodeName + "] Setting Default Names for Objects");
        setDefaultNames();

        System.out.println("[" + nodeName + "] Initializing tracing publisher.");
        tracePublisher = connectedNode.newPublisher("DinerosTracing", String._TYPE);

        System.out.println("[" + nodeName + "] Initializing servers");
        // init service servers mechanism
        // is a 1:1 mapping, because there is always just on server
        HashMap<java.lang.String, ArrayList<java.lang.String>> channelServerReqElementMap
                = petriNet.getChannelElemensByKey(PnmlConstants.CHANNEL_PLACE_TYPE_SERVER_REQ_KEY);

        for (java.lang.String key : channelServerReqElementMap.keySet()) {

            if(petriNet.getPlaceById(channelServerReqElementMap.get(key).get(0)) == null){
                System.out.println("Skipping service request place: " + channelServerReqElementMap.get(key).get(0));
                continue;
            }

            for (Page p : petriNet.allPages()) {
                if (p.getServiceName() != null && p.getServiceName().equals(key)) {
                    // service names are unique so we basically have a 1:1 mapping
                    int limit = petriNet.getChannelElementLimitById(channelServerReqElementMap.get(key).get(0));

                    for (int i = 0; i < limit; i++) {

                        System.out.println("[" + nodeName + "] Copying server page " + key + " (" + i + ").");
                        Page copy = ServiceInstanceUtil.treeCopyWithoutArcs(petriNet, p);
                        ServiceInstanceUtil.updateInstanceIds(copy, "-INSTANCE-" + i);
                        copy.setId(p.getId() + "-INSTANCE-" + i);
                        copy.getName().setText(copy.getId());
                        ServiceInstanceUtil.reintegrateArcs(p, copy, "-INSTANCE-" + i);
                        petriNet.addPage(copy);

                        try {
                            System.out.println("[" + nodeName + "] --> Initializing BalloonMarking for " + copy.getId());
                            marking.initializePageBalloonMarking(copy, false);
                            System.out.println("[" + nodeName + "] --> Initializing CallbackStorage for " + copy.getId());
                            callbackStorage.initializePageCallbackStorage(copy, petriNet, false);
                            System.out.println("[" + nodeName + "] --> Finished Initialization CallbackStorage for " + copy.getId());
                        } catch (IOException | ParserConfigurationException | SAXException e) {
                            e.printStackTrace();
                        }
                    }

                    petriNet.flushTreeCache();
                    petriNet.flushAttrAndCollectionCache();

                    marking.flushTreeCache();
                    marking.flushAttrAndCollectionCache();

                    callbackStorage.flushTreeCache();
                    callbackStorage.flushAttrAndCollectionCache();

                    HashMap<java.lang.String, ArrayList<java.lang.String>> channelServerResElementMap
                            = petriNet.getChannelElemensByKey(PnmlConstants.CHANNEL_PLACE_TYPE_SERVER_RES_KEY);

                    for (java.lang.String resKey : channelServerResElementMap.keySet()) {
                        for (int i = 0; i < limit; i++) {
                            responsePlaces.add(channelServerResElementMap.get(resKey).get(0) + "-INSTANCE-" + i);
                        }
                    }

                    connectedNode.newServiceServer(key, StringService._TYPE,
                            (ServiceResponseBuilder<StringServiceRequest, StringServiceResponse>) (request, response) -> {

                                System.out.println("[" + nodeName + "] Assigning instance to request on service " + key);
                                java.lang.String selectedInstanceInId = null;
                                java.lang.String selectedInstanceOutId = null;
                                while(selectedInstanceInId == null) {
                                    synchronized (this) {
                                        for (int i = 0; i < limit; i++) {
                                            java.lang.String iterInInstanceId =
                                                    channelServerReqElementMap.get(key).get(0) + "-INSTANCE-" + i;
                                            java.lang.String iterOutInstanceId =
                                                    channelServerResElementMap.get(key).get(0) + "-INSTANCE-" + i;
                                            if (!activeServerInstances.contains(iterOutInstanceId)) {
                                                selectedInstanceInId = iterInInstanceId;
                                                selectedInstanceOutId = iterOutInstanceId;
                                                activeServerInstances.add(iterOutInstanceId);
                                                break;
                                            }
                                        }
                                    }
                                }

                                System.out.println("[" + nodeName + "] Inserting request in " + selectedInstanceInId);
                                TracePublishing.publish(TracePublishing.TraceTypeServiceServerReceive,
                                        this.nodeName + " " + key + " " + selectedInstanceInId, this.connectedNode, this.tracePublisher);
                                Place instanceEntryPlace = petriNet.getPlaceById(selectedInstanceInId);
                                marking.resolveBalloonPlace(instanceEntryPlace).addBalloonMarking(
                                        new BalloonToken(request.getInput(), System.currentTimeMillis()));
                                this.notify(new DiNeRosEvent(DiNeRosEventTypes.NOTIFICATION_MARKING_CHANGE));

                                while(activeServerInstances.contains(selectedInstanceOutId)){}
                                System.out.println("[" + nodeName + "] Instance " + selectedInstanceOutId + " is ready to return");
                                TracePublishing.publish(TracePublishing.TraceTypeServiceServerSend,
                                        this.nodeName + " " + key + " " + selectedInstanceOutId, this.connectedNode, this.tracePublisher);

                                for(java.lang.String entry : responsePlaces){
                                    if(entry.equals(selectedInstanceOutId)){
                                        // set response value based on output token
                                        response.setOutput(marking.resolveBalloonPlace(petriNet.
                                                getPlaceById(entry)).getBalloonMarking(0).getValue());
                                        marking.resolveBalloonPlace(petriNet.
                                                getPlaceById(entry)).getBalloonMarking(0).removeSelf();
                                        break;
                                    }
                                }
                            });
                    break;
                }
            }
        }

        // init publishers
        System.out.println("[" + nodeName + "] Initializing publishers");
        HashMap<java.lang.String, ArrayList<java.lang.String>> channelPubElementMap
                = petriNet.getChannelElemensByKey(PnmlConstants.CHANNEL_PLACE_TYPE_PUB_KEY);

        for (java.lang.String key : channelPubElementMap.keySet()) {
            for (java.lang.String placeId : channelPubElementMap.get(key)) {
                if(petriNet.getPlaceById(placeId) != null) {
                    System.out.println("[" + nodeName + "] Initializing publisher on " + placeId);
                    final Publisher<String> publisher = connectedNode.newPublisher(key, String._TYPE);
                    dinerosPublishers.put(petriNet.getPlaceById(placeId).asDinerosPlace(), publisher);
                }
            }
        }

        // init subscribers
        System.out.println("[" + nodeName + "] Initializing subscribers");
        HashMap<java.lang.String, ArrayList<java.lang.String>> channelSubElementMap
                = petriNet.getChannelElemensByKey(PnmlConstants.CHANNEL_PLACE_TYPE_SUB_KEY);

        for (java.lang.String key : channelSubElementMap.keySet()) {
            for (java.lang.String placeId : channelSubElementMap.get(key)) {
                if(petriNet.getPlaceById(placeId) != null) {

                    DinerosPlace targetPlace = petriNet.getPlaceById(placeId).asDinerosPlace();
                    Subscriber<String> subscriber = connectedNode.newSubscriber(key, String._TYPE);

                    subscriber.addMessageListener(message -> {

                        System.out.println("Received new message on " + key + " : " + message.getData());

                        BalloonToken bt = new BalloonToken();
                        bt.setValue(message.getData());
                        bt.setCreationTime(System.currentTimeMillis());

                        BalloonMarkedPlace bmp = marking.resolveBalloonPlace(targetPlace);
                        bmp.getBalloonMarkingList().add(bt);
                        petriNet.flushTreeCache();

                        TracePublishing.publish(TracePublishing.TraceTypeTopicReceive,
                                this.nodeName + " " + key + " " + placeId, this.connectedNode, this.tracePublisher);

                        if(!marking.callServiceIfRequest(bmp, self)){
                            this.notify(new DiNeRosEvent(DiNeRosEventTypes.NOTIFICATION_MARKING_CHANGE));
                        }

                    }, petriNet.getChannelElementLimitById(placeId));
                }
            }
        }

        System.out.println("[" + nodeName + "] Executing first marking query.");
        this.notify(new DiNeRosEvent(DiNeRosEventTypes.NOTIFICATION_STARTUP_ENDED));
        this.internalNodeLoop();
    }

    private void setDefaultNames() {
        for (PnObject pnObject : petriNet.allObjects()) {
            if (!pnObject.hasName()) {
                Name name = new Name();
                name.setText(pnObject.getId());
                pnObject.setName(name);
            }
        }

        petriNet.flushTreeCache();
    }

    public Publisher<String> getPublisherByPlaceId(java.lang.String placeId) {
        for (DinerosPlace dp : dinerosPublishers.keySet()) {
            if (dp.getId().equals(placeId)) {
                return dinerosPublishers.get(dp);
            }
        }
        return null;
    }

    @NotNull
    protected TransitionSelectionResult fireRandomEnabledTransition(List<Transition> enabledTransitions) {
        if(enabledTransitions.size() == 0){ return new FiringSelectionNone(); }
        FiringSelectionSuccess fsc = new FiringSelectionSuccess();
        fsc.setTransition(enabledTransitions.get(new Random().nextInt(enabledTransitions.size())));
        return fsc;
    }

    protected Map<java.lang.String, Object> parseToken(java.lang.String s) {
        Gson gson = new GsonBuilder().create();
        Type mapType = new TypeToken<Map<java.lang.String, Object>>(){}.getType();
        return gson.fromJson(s, mapType);
    }

    public boolean isServerInstancePlace(java.lang.String placeId) {
        return responsePlaces.contains(placeId);
    }
}