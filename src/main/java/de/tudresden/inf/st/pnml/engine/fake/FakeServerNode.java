package de.tudresden.inf.st.pnml.engine.fake;

import org.ros.namespace.GraphName;
import org.ros.node.AbstractNodeMain;
import org.ros.node.ConnectedNode;
import org.ros.node.service.ServiceResponseBuilder;
import rosjava_srv.StringService;
import rosjava_srv.StringServiceRequest;
import rosjava_srv.StringServiceResponse;

public class FakeServerNode extends AbstractNodeMain {

    private java.lang.String serviceName;
    private java.lang.String nodeName;

    public FakeServerNode(String serviceName, String nodeName) {
        this.serviceName = serviceName;
        this.nodeName = nodeName;
    }

    @Override
    public GraphName getDefaultNodeName() {
        return GraphName.of("fake/" + nodeName);
    }

    @Override
    public void onStart(final ConnectedNode connectedNode) {

        System.out.println("[" + nodeName + "] Starting node.");

        connectedNode.newServiceServer(serviceName, StringService._TYPE,
                (ServiceResponseBuilder<StringServiceRequest, StringServiceResponse>) (request, response) -> {

                    System.out.println("[" + nodeName + "] Service Input: \"" + request.getInput() + "\"");
                    try {
                        Thread.sleep(4000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("[" + nodeName + "] Finished sleep.");
                    response.setOutput(request.getInput());
                });
    }
}
