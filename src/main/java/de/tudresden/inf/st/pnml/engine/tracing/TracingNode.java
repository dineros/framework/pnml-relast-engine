package de.tudresden.inf.st.pnml.engine.tracing;

import org.ros.namespace.GraphName;
import org.ros.node.AbstractNodeMain;
import org.ros.node.ConnectedNode;
import org.ros.node.topic.Subscriber;
import std_msgs.String;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class TracingNode extends AbstractNodeMain {

    @Override
    public GraphName getDefaultNodeName() {
        return GraphName.of("DinerosTracingNode");
    }

    @Override
    public void onStart(final ConnectedNode connectedNode) {

        File f = new File("DinerosTrace-" + System.currentTimeMillis() + ".txt");

        try {
            if (f.createNewFile()) {
                System.out.println("[Trace] Created trace file: " + f.getName());
            } else {
                System.out.println("[Trace] Trace file already exists: " + f.getName());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Subscriber<String> subscriber = connectedNode.newSubscriber("DinerosTracing", String._TYPE);

        subscriber.addMessageListener(message -> {
            java.lang.String[] splitEntry = message.getData().split(",");
            updateTrace(new TraceEntry(splitEntry[0], splitEntry[1]), f);
        }, 1000);

    }

    private synchronized void updateTrace(TraceEntry traceEntry, File traceFile){

        try {
            FileWriter writer = new FileWriter(traceFile.getName(), true);
            java.lang.String line = System.currentTimeMillis() + " " +
                    traceEntry.traceType + " " + traceEntry.traceValue;
            writer.write(line + "\n");
            writer.close();
            System.out.println("[Trace] " + line);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
