package de.tudresden.inf.st.pnml.engine.ros;

import de.tudresden.inf.st.pnml.jastadd.model.*;

public class DiNeRosNodeUtil {

    public static boolean defaultSubNetExecute(BalloonMarking balloonMarking, BalloonCallbackStorage balloonCallbackStorage,
                                               PetriNet petriNet, String subnet, InputSignalConnector inputSignalConnector, DiNeRosNode node) {

        boolean hasFired = false;

        while(hasEnabledTransition(petriNet, subnet, balloonMarking)) {
            for (Transition transition : petriNet.allTransitions()) {
                if (transition.asDinerosTransition().getStaticTransitionInformation().getSubNet().equals(subnet) &&
                        transition.asDinerosTransition().getStaticTransitionInformation().isSignalTransitionInformation()) {
                    if (balloonMarking.isBalloonEnabled(transition)) {
                        System.out.println("Firing transition: " + transition.getId());
                        balloonMarking.fireTransition(transition, balloonCallbackStorage, inputSignalConnector, node, true);
                        hasFired = true;
                    }
                }
            }
        }

        return hasFired;
    }

    public static boolean hasEnabledTransition(PetriNet petriNet, String subnet, BalloonMarking balloonMarking){

        for(Transition transition : petriNet.allTransitions()) {
            if (transition.asDinerosTransition().getStaticTransitionInformation().getSubNet().equals(subnet)) {
                if(balloonMarking.isBalloonEnabled(transition) && transition.asDinerosTransition().getStaticTransitionInformation().isSignalTransitionInformation()){
                    return true;
                }
            }
        }

        return false;
    }
}
